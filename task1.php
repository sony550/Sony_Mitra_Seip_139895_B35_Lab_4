
<script language ="JavaScript">

    var low = 2;
    var high = 100;
    while (low < high)
    {
        if (isPrime(low)) {
            document.write(low + "<br>");
        }
        ++low;
    }

    function isPrime(n) {
        var flag = 0;

        for(var i = 2; i <= n/2; ++i)
        {
            if(n % i == 0)
            {
                flag = 1;
                break;
            }
        }
        return flag == 0;
    }
</script>

